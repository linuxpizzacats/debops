# From debian 11
FROM debian:bullseye-slim

RUN apt update && apt install -y curl telnet iproute2 traceroute iputils-ping htop dnsutils wget openssh-client lftp procps cron

RUN useradd -u 5000 lpc

USER lpc:lpc

CMD ["bash"]
