# Debian for operations

Dockerfiles from debian-slim base image for working with troubleshooting at the same level of the rest of containers.
Very practical for Kubernetes networking troubleshooting or some complex cron job.

## Tags

- buster
- bullseye
- latest & main => bullseye

## GitLab registry

[linuxpizzacats/debops](https://registry.gitlab.com/linuxpizzacats/debops)

## Quay.io registry

[linuxpizzacats/debops](https://quay.io/repository/linuxpizzacats/debops)

## Tools

Usually, installed tools are:

- curl
- dnsutils 
- htop
- iproute2
- iputils-ping
- openssh-client 
- telnet
- traceroute
- wget 
- lftp
- procps
- cron

## User

Image is set to run as lpc:lpc. A user and group without privileges.
